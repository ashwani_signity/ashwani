#import "ApplicationMods.h"

@implementation ApplicationMods

+ (NSArray*) compiledMods
{
	NSMutableArray *modules = [NSMutableArray array];
	[modules addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"ti.cloud",@"name",@"ti.cloud",@"moduleid",@"2.3.0",@"version",@"1056b5d2-2bb5-4339-b930-297637aeec4e",@"guid",@"",@"licensekey",nil]];
	[modules addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"tibar",@"name",@"tibar",@"moduleid",@"0.4.2",@"version",@"cc8a85bf-eeee-4af9-a4bc-4315b8e078c3",@"guid",@"",@"licensekey",nil]];
	return modules;
}

@end
