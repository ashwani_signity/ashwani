/**
* Appcelerator Titanium Mobile
* This is generated code. Do not modify. Your changes *will* be lost.
* Generated code is Copyright (c) 2009-2011 by Appcelerator, Inc.
* All Rights Reserved.
*/
#import <Foundation/Foundation.h>
#import "TiUtils.h"
#import "ApplicationDefaults.h"
 
@implementation ApplicationDefaults
  
+ (NSMutableDictionary*) copyDefaults
{
    NSMutableDictionary * _property = [[NSMutableDictionary alloc] init];

    [_property setObject:[TiUtils stringValue:@"tJHExMF7XBoSgwcr5pzzgHYmCiec8buF"] forKey:@"acs-oauth-secret-production"];
    [_property setObject:[TiUtils stringValue:@"rMbFGgkqy3e9BIIe2c3k5T33hPllmUDy"] forKey:@"acs-oauth-key-production"];
    [_property setObject:[TiUtils stringValue:@"6k0r9hEtdVNT8nFE5ZkKlFuDkCrAkMHr"] forKey:@"acs-api-key-production"];
    [_property setObject:[TiUtils stringValue:@"Jg3hN14x96PNUOfQ4kW6BfoMcg0blvDg"] forKey:@"acs-oauth-secret-development"];
    [_property setObject:[TiUtils stringValue:@"s4p32UJSZFhc76HIXnWF4kJUzl7Qou51"] forKey:@"acs-oauth-key-development"];
    [_property setObject:[TiUtils stringValue:@"hPhHKeLqgnbIdLs5pd0KcxI4jKeGUkeS"] forKey:@"acs-api-key-development"];
    [_property setObject:[TiUtils stringValue:@"system"] forKey:@"ti.ui.defaultunit"];

    return _property;
}
@end
