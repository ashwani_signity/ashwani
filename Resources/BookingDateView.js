 // Taking Screen Width
var screenWidth = 322;
var needToChangeSize = false;
var currentValue = new Date();
var currentMonth = currentValue.getMonth()+1;
var currentYear  = currentValue.getFullYear();
var valueArray = [];
var monthCustomValue;
if(currentMonth==13){
	currentMonth=1;
}

var timeValue;

var screenWidthActual = Ti.Platform.displayCaps.platformWidth;

if (Ti.Platform.osname === 'android') {
if (screenWidthActual >= 641) {
screenWidth = screenWidthActual;
needToChangeSize = true;
}
}

// Main Window of the Month View.
var win=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#fff');
win.fullscreen = false;



var bookingHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center',
	text:'Book Now'
});
win.add(bookingHeader);

var backViewButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
win.add(backViewButton);
backViewButton.addEventListener('click',function(e){
	win.navGroup.close(win);
});



var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:390,
	width:'100%',
	top:44
});
win.add(centerView);


function loadCalendar(calArray){


// Button at the buttom side
var backButton = Ti.UI.createButton({
bottom : '100dp',
height : '40dp',
width : '200dp'
});

// Previous Button - Tool Bar
var prevMonth = Ti.UI.createView({
left : '15dp',
width : '15',
height : '17',
backgroundImage : 'arrow-left.png'
});
prevMonth.addEventListener('click',function(e){
	if(currentMonth==1){
	currentMonth=12;
	currentYear-=1;
}else{
	currentMonth-=1;
}

loadCalendar(valueArray);
});


// Next Button - Tool Bar
var nextMonth = Ti.UI.createView({
right : '15dp',
width : '15',
height : '17',
backgroundImage:'arrow-right.png'
});
nextMonth.addEventListener('click',function(e){
	if(currentMonth==12){
	currentMonth=1;
	currentYear+=1;
}else{
	currentMonth+=1;
}

loadCalendar(valueArray);

});

// Month Title - Tool Bar
var monthTitle = Ti.UI.createLabel({
width : '200dp',
height : '24dp',
textAlign : 'center',
color : '#3a4756',
font : {
fontSize : 20,
fontWeight : 'bold'
}
});

// Tool Bar
var toolBar = Ti.UI.createView({
top : '0dp',
width : '322dp',
height : '50dp',
backgroundColor : '#FFFFD800',
layout : 'vertical'
});

// Tool Bar - View which contain Title Prev. & Next Button
var toolBarTitle = Ti.UI.createView({
top : '3dp',
width : '322dp',
height : '24dp'
});

toolBarTitle.add(prevMonth);
toolBarTitle.add(monthTitle);
toolBarTitle.add(nextMonth);

// Tool Bar - Day's
var toolBarDays = Ti.UI.createView({
top : '2dp',
width : '322dp',
height : '22dp',
layout : 'horizontal',
left : '-1dp'
});

toolBarDays.sunday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Sun',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.monday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Mon',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.tuesday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Tue',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.wednesday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Wed',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.thursday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Thu',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.friday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Fri',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.saturday = Ti.UI.createLabel({
left : '0dp',
height : '20dp',
text : 'Sat',
width : '46dp',
textAlign : 'center',
font : {
fontSize : 12,
fontWeight : 'bold'
},
color : '#3a4756'
});

toolBarDays.add(toolBarDays.sunday);
toolBarDays.add(toolBarDays.monday);
toolBarDays.add(toolBarDays.tuesday);
toolBarDays.add(toolBarDays.wednesday);
toolBarDays.add(toolBarDays.thursday);
toolBarDays.add(toolBarDays.friday);
toolBarDays.add(toolBarDays.saturday);

// Adding Tool Bar Title View & Tool Bar Days View
toolBar.add(toolBarTitle);
toolBar.add(toolBarDays);

// Function which create day view template
dayView = function(e) {
var label = Ti.UI.createLabel({
current : e.current,
width : '46dp',
height : '44dp',
backgroundColor : '#e3e2e5',     //FFDCDCDF
text : e.day,
textAlign : 'center',
color : e.color,
font : {
fontSize : 20,
fontWeight : 'bold'
}
});
return label;
};

centerView.add(toolBar);


monthTitle.text = monthCalculation(currentMonth)+' '+currentYear;

var daysInMonth = 32 - new Date(currentYear, currentMonth-1, 32).getDate();
var dayOfWeek = new Date(currentYear, currentMonth-1, 1).getDay();

var mainView = Ti.UI.createView({
layout : 'horizontal',
width : '320dp',
height : 'auto',
top : '50dp',
backgroundColor: '#e3e2e5'
});

centerView.add(mainView);
var dayNumber=1;
var leftvalue;
if(dayOfWeek==0){
	leftvalue=0;
}else if(dayOfWeek==1){
	leftvalue=45.7;
}else if(dayOfWeek==2){
	leftvalue=91.4;
}else if(dayOfWeek==3){
	leftvalue=137.1;
}else if(dayOfWeek==4){
	leftvalue=182.8;
}else if(dayOfWeek==5){
	leftvalue=228.5;
}else if(dayOfWeek==6){
	leftvalue=274.2
}

for(i=0; i<daysInMonth; i++){
	
var newDay = Ti.UI.createLabel({
	left:leftvalue,
	height:'45.7',
	width:'45.7',
	backgroundColor:'red',
	text:dayNumber,
	textAlign:'center',
	dayOfWeek:dayOfWeek
});
mainView.add(newDay);


for(j=0; j<calArray.length; j++){
	if(newDay.text==calArray[j].day && currentMonth==calArray[j].month && currentYear==calArray[j].year){
		newDay.backgroundColor='green';
		newDay.addEventListener('click',function(e){
			Titanium.API.info(e.source.text);
			Titanium.App.Properties.setString('BookDate',currentYear+'-'+currentMonth+'-'+e.source.text);
			settingTime(currentYear,currentMonth,e.source.text);
		});
	}
}
	dayNumber++;
	leftvalue=0;		
}


//Titanium.API.info(daysInMonth+' in '+currentMonth+' '+dayOfWeek);

}




/*


monthName = function(e) {
switch(e) {
case 0:
e = 'January';
break;
case 1:
e = 'February';
break;
case 2:
e = 'March';
break;
case 3:
e = 'April';
break;
case 4:
e = 'May';
break;
case 5:
e = 'June';
break;
case 6:
e = 'July';
break;
case 7:
e = 'August';
break;
case 8:
e = 'September';
break;
case 9:
e = 'October';
break;
case 10:
e = 'November';
break;
case 11:
e = 'December';
break;
};
return e;
};

// Calendar Main Function
var calView = function(a, b, c) {
//create main calendar view
var mainView = Ti.UI.createView({
layout : 'horizontal',
width : '322dp',
height : 'auto',
top : '50dp',
backgroundColor: '#e3e2e5'
});

//set the time
var daysInMonth = 32 - new Date(a, b, 32).getDate();
var dayOfMonth = new Date(a, b, c).getDate();
var dayOfWeek = new Date(a, b, 1).getDay();
var daysInLastMonth = 32 - new Date(a, b - 1, 32).getDate();
var daysInNextMonth = (new Date(a, b, daysInMonth).getDay()) - 6;

//set initial day number
var dayNumber = daysInLastMonth - dayOfWeek + 1;

//get last month's days
for ( i = 0; i < dayOfWeek; i++) {
mainView.add(new dayView({
day : dayNumber,
color : '#8e959f',
current : 'no',
dayOfMonth : ''
}));
dayNumber++;
};

// reset day number for current month
dayNumber = 1;

//get this month's days
for ( i = 0; i < daysInMonth; i++) {	
var newDay = new dayView({
day : dayNumber,
 color : '#3a4756',
current : 'yes',
dayOfMonth : dayOfMonth
});
mainView.add(newDay);

for(j=0; j<calArray; j++){
	
if(newDay.text == calArray[j].day ){
	
}
}


if(newDay.text < dayOfMonth){



}else if(newDay.text == dayOfMonth){
	// newDay.backgroundColor = '#555555';
	// newDay.color='#fff';

	
}else if (newDay.text > dayOfMonth) {  
	
	
	
var oldDay = newDay;

}
dayNumber++;
};
dayNumber = 1;
//get remaining month's days
for ( i = 0; i > daysInNextMonth; i--) {
mainView.add(new dayView({
day : dayNumber,
color : '#8e959f',
current : 'no',
dayOfMonth : ''
}));
dayNumber++;
};



// this is the new "clicker" function, although it doesn't have a name anymore, it just is.
mainView.addEventListener('click', function(e) {
if (e.source.current == 'yes') {

// reset last day selected
if(oldDay.text==dayOfMonth){
	
	
	
	oldDay.backgroundColor = '#555555';
	oldDay.color='#fff';
	

}else if (oldDay.text > dayOfMonth) {
	
	
		oldDay.color = 'white';
       oldDay.backgroundColor = '#2e8418';
     

} else {
oldDay.color = '#3a4756';     
//oldDay.backgroundImage='../libraries/calendar/pngs/monthdaytile-Decoded.png';
//oldDay.backgroundColor = '#FFDCDCDF'
}
oldDay.backgroundPaddingLeft = '0dp';
oldDay.backgroundPaddingBottom = '0dp';

// set characteristic of the day selected
if (e.source.text >= dayOfMonth) {
	
	
// e.source.backgroundImage='../libraries/calendar/pngs/monthdaytiletoday_selected.png';



e.source.backgroundColor = 'red';
} else {
// e.source.backgroundImage='../libraries/calendar/pngs/monthdaytile_selected.png';
//e.source.backgroundColor = '#FFFF0000';
}
e.source.backgroundPaddingLeft = '1dp';
e.source.backgroundPaddingBottom = '1dp';
//e.source.color = 'white';
//this day becomes old :(
oldDay = e.source;

// set window title with day selected, for testing purposes only
//backButton.title = nameOfMonth + ' ' + e.source.text + ', ' + a;

Titanium.API.info(nameOfMonth + ' ' + e.source.text + ', ' + a);
if (e.source.text >= dayOfMonth) {


}


}
});


return mainView;
};


//####
//############# Picker View for Time
//####
var transformPicker = Titanium.UI.create2DMatrix().scale(1.1);
var timePicker = Titanium.UI.createPicker({
	
	transform:transformPicker,
	top:550
});

var data = [];
data[0] = Ti.UI.createPickerRow({
    title : 'Bananas'
});
data[1] = Ti.UI.createPickerRow({
    title : 'Strawberries'
});
data[2] = Ti.UI.createPickerRow({
    title : 'Mangos'
});
data[3] = Ti.UI.createPickerRow({
    title : 'Grapes'
});

timePicker.add(data);
timePicker.selectionIndicator = true;
 
win.add(timePicker);

//####
//############# Custom Loader
//####


loader = function(){

var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

win.add(transview);

//Code for showing loader----------------------------------------END-----------------------

setTimeout(function(){
	win.remove(transview);	
},5000);

};



// what's today's date?
var setDate = new Date();
a = setDate.getFullYear();
b = setDate.getMonth();
c = setDate.getDate();

// add the three calendar views to the window for changing calendars with animation later

var prevCalendarView = null;
if (b == 0) {
prevCalendarView = calView(a - 1, 11, c);
} else {
prevCalendarView = calView(a, b - 1, c);
}
prevCalendarView.left = (screenWidth * -1) + 'dp';

var nextCalendarView = null;
if (b == 0) {
nextCalendarView = calView(a + 1, 0, c);
} else {
nextCalendarView = calView(a, b + 1, c);
}
nextCalendarView.left = screenWidth + 'dp';

var thisCalendarView = calView(a, b, c);
if (needToChangeSize == false) {
thisCalendarView.left = '-1dp';
}



monthTitle.text = monthName(b) + ' ' + a;

backButton.title = monthName(b) + ' ' + c + ', ' + a;



// add everything to the window
centerView.add(toolBar);
centerView.add(thisCalendarView);
centerView.add(nextCalendarView);
centerView.add(prevCalendarView);
//centerView.add(backButton);

// yeah, open the window, why not?
// win.open({
// //modal : true
// });

var slideNext = Titanium.UI.createAnimation({
// left : '-322',
duration : 500
});

slideNext.left = (screenWidth * -1);

var slideReset = Titanium.UI.createAnimation({
// left : '-1',
duration : 500
});

if (needToChangeSize == false) {
slideReset.left = '-1';
} else {
slideReset.left = ((screenWidth - 644) / 2);
}

var slidePrev = Titanium.UI.createAnimation({
// left : '322',
duration : 500
});

slidePrev.left = screenWidth;

// Next Month Click Event
nextMonth.addEventListener('click', function() {

if (b == 11) {
b = 0;
a++;
} else {
b++;
}

if(currentMonth==12){
	currentMonth=1;
	currentYear+=1;
}else{
	
	currentMonth+=1;
}

thisCalendarView.animate(slideNext);
nextCalendarView.animate(slideReset);

setTimeout(function() {
	
thisCalendarView.left = (screenWidth * -1) + 'dp';
if (needToChangeSize == false) {
nextCalendarView.left = '-1dp';
} else {
nextCalendarView.left = ((screenWidth - 644) / 2);
}
prevCalendarView = thisCalendarView;
thisCalendarView = nextCalendarView;
if (b == 11) {
nextCalendarView = calView(a + 1, 0, c);
} else {
nextCalendarView = calView(a, b + 1, c);
}
monthTitle.text = monthName(b) + ' ' + a;
nextCalendarView.left = screenWidth + 'dp';
centerView.add(nextCalendarView);
}, 500);


//loadCalendar(valueArray);

});

// Previous Month Click Event
prevMonth.addEventListener('click', function() {
	
if (b == 0) {
b = 11;
a--;
} else {
b--;
}

if(currentMonth==1){
	currentMonth=12;
	currentYear-=1;
}else{
	
	currentMonth-=1;
}


thisCalendarView.animate(slidePrev);
prevCalendarView.animate(slideReset);
setTimeout(function() {
thisCalendarView.left = screenWidth + 'dp';
if (needToChangeSize == false) {
prevCalendarView.left = '-1dp';
} else {
prevCalendarView.left = ((screenWidth - 644) / 2);
}
nextCalendarView = thisCalendarView;
thisCalendarView = prevCalendarView;
if (b == 0) {
prevCalendarView = calView(a - 1, 11, c);
} else {
prevCalendarView = calView(a, b - 1, c);
}
monthTitle.text = monthName(b) + ' ' + a;
prevCalendarView.left = (screenWidth * -1) + 'dp';
centerView.add(prevCalendarView);
}, 500);


//loadCalendar(valueArray);

});

}

*/

function checkForAvailableDates() {
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

win.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/services.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              //Ti.API.info(str_res);
              var arry = [];
              var monthArray = {};
              var dayArray = [];
              
              for(var i=0; i<str_res.data.length;i++){
              	arry.push(str_res.data[i].split("-"));
              	
              }
              for(var j=0; j<arry.length; j++){
              	monthArray = ({year:arry[j][0], month:arry[j][1], day:arry[j][2]});
              	valueArray.push(monthArray);
              }
              
             
              loadCalendar(valueArray);
             
             //Titanium.API.info(monthCalculation(valueArray));
              
			  win.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      win.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
win.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'available_bookingDate', webSiteId:'7', service_id:Ti.App.Properties.getString('SerId'), provider_id:Ti.App.Properties.getString('techId') });
/*setTimeout(function(){
	apiData();
},1000);*/	
}


function settingTime(year,month,date){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

win.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/services.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              if(str_res.success == 1){
              timeValue = str_res.data[0];
              settingPicker(str_res);
              }
			  win.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      win.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
win.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'bookingTime', webSiteId:'7', service_id:Ti.App.Properties.getString('SerId'), selected_date:year+'-'+month+'-'+date, provider_id:Ti.App.Properties.getString('techId') });
/*setTimeout(function(){
	apiData();
},1000);*/
	
}



//####
//############# Picker View for Time
//####

function settingPicker(dataArray){
	
var toolbarView = Ti.UI.createView({
	top:250,
	height:50,
	backgroundColor:'gray'
});	
win.add(toolbarView);

var doneButton = Ti.UI.createLabel({
	top:12,
	left:5,
	height:30,
	width:40,
	text:'Done',
	color:'#fff',
	backgroundColor:'#000',
	font:{fontWeight:'bold',fontSize:12},
	textAlign:'center',
	borderRadius:'7'
});
toolbarView.add(doneButton);
doneButton.addEventListener('click',function(e){
	var animation = Ti.UI.createAnimation({
		duration:500,
		top:550
	});
	
	toolbarView.animate(animation);
	timePicker.animate(animation);
	
	Titanium.App.Properties.setString('BookTime',timeValue);
	Ti.API.info(timeValue);
	
	var BookRegWin = Ti.UI.createWindow({
		url:'BookingReg.js',
		_parent:Titanium.UI.currentWindow,
		navGroup: win.navGroup,
		rootWindow : win.rootWindow
	});
	win.navGroup.open(BookRegWin);
	
});
	
var transformPicker = Titanium.UI.create2DMatrix().scale(1);
var timePicker = Titanium.UI.createPicker({
	
	transform:transformPicker,
	top:300
});

var data = [];

for(var i=0; i<dataArray.data.length; i++){

var pickerRow = Ti.UI.createPickerRow({
	title:dataArray.data[i]
});
data.push(pickerRow);

}
timePicker.add(data);
timePicker.selectionIndicator = true;
 
timePicker.addEventListener('change',function(e){
	var newTime = dataArray.data[e.rowIndex];
	timeValue=newTime;
}) ;
 
win.add(timePicker);
}

function monthCalculation(m){
 switch(m){
case 1:
m = 'January';
break;
case 2:
m = 'February';
break;
case 3:
m = 'March';
break;
case 4:
m = 'April';
break;
case 5:
m = 'May';
break;
case 6:
m = 'June';
break;
case 7:
m = 'July';
break;
case 8:
m = 'August';
break;
case 9:
m = 'September';
break;
case 10:
m = 'October';
break;
case 11:
m = 'November';
break;
case 12:
m = 'December';
break;
};
return m;	
	
}

checkForAvailableDates();


