var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);

var providerHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center',
	text:Titanium.App.Properties.getString('technician')
});
centerView.add(providerHeader);

var backButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
centerView.add(backButton);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});


var profilePic = Ti.UI.createImageView({
	url:Titanium.App.Properties.getString('techImage'),
	top:10,
	left:10,
	height:150,
	width:150
});


var typeLabel = Ti.UI.createLabel({
	backgroundColor:'#fff',
	color:'#ff6000',
	font:{fontWeight:'bold',fontSize:'20'},
	top:10,
	left:180,
	height:50,
	width:100,
	textAlign:'center',
	text:'Services'
});

var expertLabel = Ti.UI.createLabel({
	backgroundColor:'#fff',
	color:'#000',
	text:Titanium.App.Properties.getString('techSer'),
	top:50,
	left:'180',
	height:20,
	font:{fontSize:14}
});



var bookingButton = Ti.UI.createLabel({
	backgroundColor:'green',
	color:'#fff',
	width:200,
	height:50,
	top:160,
	left:60,
	text:'Book Now',
	font:{fontWeight:'bold',fontSize:18},
	textAlign:'center',
	borderRadius:'7'
});
bookingButton.addEventListener('click',function(e){
	var BookingDateView=Titanium.UI.createWindow({	
		url:'BookingDateView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : MainWin.navGroup,
        rootWindow : MainWin.currentWindow
	});
	MainWin.navGroup.open(BookingDateView);
	
});


var bioLabel = Ti.UI.createLabel({
	backgroundColor:'#fff',
	color:'#ff6000',
	text:'Bio',
	top:220,
	left:'10',
	height:20,
	font:{fontWeight:'bold',fontSize:16}
});

var descLabel = Ti.UI.createLabel({
	backgroundColor:'#fff',
	color:'#000',
	text:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using , making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for  will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose',
	top:240,
	left:'10',
	width:300,
	font:{fontSize:16}
});


var bodyView = Ti.UI.createScrollView({
	backgroundColor:'#fff',
	// showPagingControl:'true',
	// pagingControlColor:'red',
	top:44,
	height:386
});


bodyView.add(profilePic);
bodyView.add(typeLabel);
bodyView.add(expertLabel);
bodyView.add(bookingButton);
bodyView.add(bioLabel);
bodyView.add(descLabel);
MainWin.add(bodyView);

