var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;
MainWin.navBarHidden=true;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Contact Us',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

centerView.add(backButton);

var infoLabel = Titanium.UI.createLabel({
	backgroundColor:'#e6e6e6',
	top:44,
	left:0,
	width:320,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'18'},
	
});
centerView.add(infoLabel);

var addrLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	top:70,
	left:30,
	width:250,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
	color:'#000'
});
centerView.add(addrLabel);


/*
var addrLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Address',
	top:100,
	left:30,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
	color:'#ff6000'
});
centerView.add(addrLabel);

var addrValueLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	top:100,
	left:110,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
});
centerView.add(addrValueLabel);

var phoneLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Phone',
	top:120,
	left:30,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
	color:'#ff6000'
});
centerView.add(phoneLabel);

var phoneValueLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	top:120,
	left:110,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
});
centerView.add(phoneValueLabel);

var emailLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Email',
	top:140,
	left:30,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
	color:'#ff6000'
});
centerView.add(emailLabel);

var emailValueLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	top:140,
	left:110,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'16'},
});
centerView.add(emailValueLabel);

*/

var callUsButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:200,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(callUsButton);

var callUsText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Call us',
    color:'#fff'
});
callUsButton.add(callUsText);

var emailButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:260,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(emailButton);

var emailText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Email us',
    color:'#fff'
});
emailButton.add(emailText);

var getDirectionButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:320,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(getDirectionButton);

var directionText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Get Directions',
    color:'#fff'
});
getDirectionButton.add(directionText);

function addressFunction(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/information.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
             if(str_res.success==1){
             	
             	infoLabel.text=str_res.data[0].title;
             	addrLabel.text='Address: '+str_res.data[0].address+'\n'+'Phone Number: '+str_res.data[0].phone_number+'\n'+'Email: '+str_res.data[0].email_address;
             }
             
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'contact_us', webSiteId:'15'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}

addressFunction();


