var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var RegisterWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	navGroup: MainWin.navGroup,
	rootWindow : MainWin.rootWindow
});

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:436,
	width:'100%'
});
RegisterWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Events',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
MainWin.navGroup.close(MainWin);
});

headerView.add(backButton);

var infoLabel = Titanium.UI.createLabel({
	backgroundColor:'#e6e6e6',
	text:Titanium.App.Properties.getString('EventName'),
	top:44,
	left:0,
	width:320,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'18'},
	
});
centerView.add(infoLabel);

var aboutUsImage = Titanium.UI.createImageView({
	url:'dummy.png',
	top:80,
	left:10,
	height:150,
	width:150
});
centerView.add(aboutUsImage);

var detailLabel = Titanium.UI.createLabel({
	text:'Details',
	top:95,
	left:170,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(detailLabel);

var startLabel = Titanium.UI.createLabel({
	text:'Starts',
	top:110,
	left:170,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(startLabel);

var startTimeLabel = Titanium.UI.createLabel({
	text:'27th Nov 2012',
	top:110,
	left:210,
	font:{fontWeight:'bold',fontSize:'12'}
});
centerView.add(startTimeLabel);

var endLabel = Titanium.UI.createLabel({
	text:'Ends',
	top:125,
	left:170,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(endLabel);

var endTimeLabel = Titanium.UI.createLabel({
	text:'Ongoing Sunday',
	top:125,
	left:210,
	font:{fontWeight:'bold',fontSize:'12'}
});
centerView.add(endTimeLabel);

var timeLabel = Titanium.UI.createLabel({
	text:'Time',
	top:160,
	left:170,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(timeLabel);

var timeValueLabel = Titanium.UI.createLabel({
	text:'8 PM',
	top:160,
	left:210,
	font:{fontWeight:'bold',fontSize:'12'}
	
});
centerView.add(timeValueLabel);

var vanueLabel = Titanium.UI.createLabel({
	text:'At',
	top:175,
	left:170,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(vanueLabel);

var vanueValueLabel = Titanium.UI.createLabel({
	text:'Woombye Pub',
	top:175,
	left:210,
	font:{fontWeight:'bold',fontSize:'12'}
});
centerView.add(vanueValueLabel);

var registerButton = Titanium.UI.createView({
	backgroundColor:'green',
	height:40,
	width:200,
	top:230,
	borderRadius:'7'
});
centerView.add(registerButton);

var registerButtonText = Titanium.UI.createLabel({
	background:'transparent',
	text:'Register to attend',
	font:{fontWeight:'bold',fontSize:'18'},
	color:'#fff'
});
registerButton.add(registerButtonText);


var moreInfo = Titanium.UI.createLabel({
	text:'More Information',
	top:275,
	left:10,
	font:{fontWeight:'bold',fontSize:'12'},
	color:'#ff6000'
});
centerView.add(moreInfo);

var infoTextView = Titanium.UI.createTextArea({
	value:Titanium.App.Properties.getString('EventDec'),
	top:290,
	left:10,
	height:170,
	width:300,
	textAlign:'left',
	editable:false
});
centerView.add(infoTextView);

MainWin.add(RegisterWin);

