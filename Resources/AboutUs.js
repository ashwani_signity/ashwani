var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;


var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'About Us',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

centerView.add(backButton);

//################
//###########################################  Image View  ##############################################//
//################

var aboutUsImage = Titanium.UI.createImageView({
	top:44,
	left:10,
	height:200,
	width:300
});

centerView.add(aboutUsImage);


//################
//###########################################  Text View  ##############################################//
//################

var aboutUsTextArea = Titanium.UI.createTextArea({
	
	top:250,
	left:5,
	height:185,
	width:310,
	textAlign:'left',
	editable:false
	
});
centerView.add(aboutUsTextArea);


function aboutFunction(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/information.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
             if(str_res.success==1){
             	aboutUsImage.url=str_res.data[0].image;
             	aboutUsTextArea.value=str_res.data[0].content;
             	
             }
             
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'about_us', webSiteId:'7'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}

aboutFunction();
