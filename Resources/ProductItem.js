var imageArray = [];

var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;


var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);

var productItemHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center',
	text:Titanium.App.Properties.getString('Category')
});
centerView.add(productItemHeader);

var cartButton = Titanium.UI.createLabel({
	height:30,
	width:80,
	top:7,
	right:10,
	text:'Cart',
	color:'#fff',
	backgroundColor:'green',
	textAlign:'center',
	borderRadius:'7',
	font:{fontWeight:'bold',fontSize:16}
});
cartButton.addEventListener('click',function(){
	var CartView = Ti.UI.createWindow({
				     url:'ShoppingCartView.js',
		        navGroup: MainWin.navGroup,
		     rootWindow : MainWin.rootWindow
			});
			MainWin.navGroup.open(CartView);
});

centerView.add(cartButton);


var backButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
centerView.add(backButton);
backButton.addEventListener('click',function(e){
	
	MainWin.navGroup.close(MainWin);
});

var productItemSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
	backgroundColor:'#343434'
});
centerView.add(productItemSearchBar);

//#################
//##################################################  Table View  #############################################//
//#################



/************************* API Connection   ************************/

function productLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = Titanium.App.Properties.getString("baseurl");
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateProductTable(str_res);
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'categoryDetail',webSiteId:'7', categoryId:Titanium.App.Properties.getString('CategoryId')});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}

productLoader();


function populateProductTable(str_res){
var productData=[];



for(var i=0; i<str_res.data.length;i++){
//alert(i);
//  alert(str_res.data[i].product_name);
  
  
	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
 if(str_res.data[i].type=='subCategory'){
 	
 	
 	var catImage = Ti.UI.createImageView({
    	url:str_res.data[i].category_image,
    	top:5,
    	left:5,
    	height:60,
    	width:50
    });
    productRow.add(catImage);
    
    var productLbl = Ti.UI.createLabel({
        text: str_res.data[i].category_name,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10
    });
    productRow.add(productLbl);
    
    
    var productDes = Ti.UI.createLabel({
    	text: str_res.data[i].category_detail,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:60,
        top:40,
        height:30,
        width:200
    });
    productRow.add(productDes);
 	
 	
 }else{
 
    // Now create each object to appear and add to row
    var proImage = Ti.UI.createImageView({
    	url:str_res.data[i].thumbnail_size,
    	top:5,
    	left:5,
    	height:60,
    	width:50
    });
    productRow.add(proImage);
  
    var productLbl = Ti.UI.createLabel({
        text: str_res.data[i].product_name,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:16},
        backgroundColor:'transparent',
        left:60,
        top:10,
        height:'20%',
        width:'60%'
        
    });
    productRow.add(productLbl);
    
    
    var productDes = Ti.UI.createLabel({
    	text: str_res.data[i].product_detail,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:60,
        top:25,
        height:30,
        width:200
    });
    productRow.add(productDes);
    // Now we add the row to our data array
    
    var proQuan = Ti.UI.createLabel({
    	text: str_res.data[i].sample,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:250,
        top:10
    });
    productRow.add(proQuan);
    
    var productPrice = Ti.UI.createLabel({
    	text: str_res.data[i].price,
        textAlign: 'left',
        backgroundColor:'transparent',
        color:'#ff6000',
        font:{fontSize:14},
        left:60,
        top:50
    });
    productRow.add(productPrice);
    
    
   }
    
    productData.push(productRow);

}

var productTable = Titanium.UI.createTableView({
       data:productData,
       top:94,
       height:336,
       rowHeight:70,
});
productTable.addEventListener('click',function(e){
	 
	 if(str_res.data[e.index].type=='subCategory'){
		Titanium.App.Properties.setString('Category',str_res.data[e.index].category_name);
		Titanium.App.Properties.setString('CategoryId',str_res.data[e.index].category_id);
		var ProductItemView=Titanium.UI.createWindow({	
		url:'ProductItem.js',
		navGroup: MainWin.navGroup,
		rootWindow : MainWin.rootWindow
	});
      MainWin.navGroup.open(ProductItemView);
  }else{
  	
   Titanium.App.Properties.setString('ProductId',str_res.data[e.index].product_id);
   Titanium.App.Properties.setString('ProductName',str_res.data[e.index].product_name);
   Titanium.App.Properties.setString('ProductPrice',str_res.data[e.index].price);
   Titanium.App.Properties.setString('ProductImage',str_res.data[e.index].thumbnail_size);
   Titanium.App.Properties.setString('ProductBrand',str_res.data[e.index].brand);
   Titanium.App.Properties.setString('ProductQuantity',str_res.data[e.index].sample);
   Titanium.App.Properties.setString('ProductDetail',str_res.data[e.index].product_detail);
  
   
    var array = ({image1:str_res.data[e.index].additional_image1, image2:str_res.data[e.index].additional_image2, image3:str_res.data[e.index].additional_image3});
   imageArray.push(array);
    Titanium.App.Properties.setList('ProductImages',imageArray);
  	var ProductCart=Titanium.UI.createWindow({	
		url:'ProductType.js',
		navGroup: MainWin.navGroup,
		rootWindow : MainWin.rootWindow
	});
      MainWin.navGroup.open(ProductCart);
  }
});
centerView.add(productTable);

}


// open window
