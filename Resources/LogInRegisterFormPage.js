var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;



var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);


//################################ Registration API #####################################//

function productLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/users.php'
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              if(str_res.success==1){
              	addUserToDB(str_res.user_id);
              	alert('User registered successfully. Please check your mail.');
              }
              
              //populateProductTable(str_res);
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='Please try Again.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'user_registeration',webSiteId:'7',first_name:firstNameTextField.value ,last_name:lastNameTextField.value ,email:emailTextField.value ,phone_number:phoneTextField.value ,street_address:streetTextField.value ,city_suburb:cityTextField.value ,state:stateTextField.value ,zip_code:pinTextField.value});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}


//###################################### Login API #################################//

function pushNotifLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/users.php'
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              if(str_res.success==1){
              	var loginAlert = Ti.UI.createAlertDialog({
              		message:'Login Success.',
              		buttonNames:['Ok']
              	});
              	loginAlert.addEventListener('click',function(e){
              		if(e.index==0){
              			MainWin.navGroup.close(MainWin);
              		}
              	});
              	loginAlert.show();
              }
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='Please try Again.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'device_info',user_id:Ti.App.Properties.getString('UID'),device_id:Ti.Platform.id,device_accesstoken:Ti.App.Properties.getString('token')});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}


function loginLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/users.php'
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              if(str_res.success==1){
              	Ti.App.Properties.setString('UID',str_res.user_id);
              	pushNotifLoader();
              }
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='Please try Again.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'login',webSiteId:'7',email:userNameTextField.value ,password:passWordTextField.value});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}




//##############################################Login Field###########################################//
//##Login Label##//
var loginLabel = Titanium.UI.createLabel({
	backgroundColor:'#FFFFFF',
	height:'8%',
	width:'50%',
	left:'4%',
	top:10,
	text:'Log In',
	textAlign:'left',
	color:'#ff6000',	
	font:{fontWeight:'bold',fontSize:'20'}
});
centerView.add(loginLabel);
//##user name field##//
var userNameTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'81%',
	top:45,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Username'
});
centerView.add(userNameTextField);
//##Passweord Field##//
var passWordTextField = Titanium.UI.createTextField({
	
	height:'6%',
	width:'81%',
	top:85,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Password',
	passwordMask:true
});
centerView.add(passWordTextField);

//##Login Button##//
var loginButton = Titanium.UI.createView({
     backgroundColor:'#00cb40',
	// left:'1.56%',
	top:120,
	height:'8%',
	width:'55%',
	borderRadius:'5'

});
centerView.add(loginButton);

var loginbuttonLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Log In',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
loginButton.add(loginbuttonLabel);
loginButton.addEventListener('click',function(){
	userNameTextField.blur();
	passWordTextField.blur();
	
	loginLoader();
});

//############################################Register Text Field##########################################//
//##Register Label##//
var registerLabel = Titanium.UI.createLabel({
	backgroundColor:'#FFFFFF',
	height:'5%',
	width:'50%',
	left:'4%',
	top:170,
	text:'Register',
	textAlign:'left',
	color:'#ff6000',	
	font:{fontWeight:'bold',fontSize:'20'}
});
centerView.add(registerLabel);
//##First Name Field##//
var firstNameTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:200,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'First Name'
});
firstNameTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-20
	});
	centerView.animate(animation);
});

firstNameTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});
centerView.add(firstNameTextField);
//##Last Name Field##//
var lastNameTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:200,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Last Name'
});

lastNameTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-20
	});
	centerView.animate(animation);
});

lastNameTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});

centerView.add(lastNameTextField);
//##EMail Field##//
var emailTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:235,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'E-mail'
});
emailTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-45
	});
	centerView.animate(animation);
});

emailTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});
centerView.add(emailTextField);
//##Phone Number Field##//
var phoneTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:235,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Phone Number'
});
phoneTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-45
	});
	centerView.animate(animation);
});

phoneTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});
centerView.add(phoneTextField);
//##Street Addresss Field##//
var streetTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'60%',
	top:270,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Street Address'
});

streetTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-80
	});
	centerView.animate(animation);
});

streetTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});

centerView.add(streetTextField);
//##Pin Field##//
var pinTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'20%',
	top:270,
	left:'67%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'4554'
});
pinTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-80
	});
	centerView.animate(animation);
});

pinTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});
centerView.add(pinTextField);

//##City Field##//
var cityTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:305,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'City/Suburb'
});

cityTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-120
	});
	centerView.animate(animation);
});

cityTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});

centerView.add(cityTextField);

//##State Field##//
var stateTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:305,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Select State'
});

stateTextField.addEventListener('focus',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:-120
	});
	centerView.animate(animation);
});

stateTextField.addEventListener('blur',function(e){
	var animation = Ti.UI.createAnimation({
		duration:100,
		top:0
	});
	centerView.animate(animation);
});
centerView.add(stateTextField);

//##Register Button##//
var registerButton = Titanium.UI.createView({
     backgroundColor:'#00cb40',
	// left:'1.56%',
	top:'78%',
	height:'8%',
	width:'55%',
	borderRadius:'5'

});
centerView.add(registerButton);

var registerbuttonLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Register',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
registerButton.add(registerbuttonLabel);
registerButton.addEventListener('click',function(){
	if(firstNameTextField.value==''){
		alert('First Name Field is compulsory.');
	}else if(lastNameTextField.value==''){
		alert('Last Name Field is compulsory.');
	}else if(emailTextField.value==''){
		alert('Email Field is compulsory.');
	}else if(phoneTextField.value==''){
		alert('Phone Number is compulsory.');
	}else if(streetTextField.value==''){
		alert('Adresss Field is compulsory.');
	}else if(pinTextField.value==''){
		alert('PIN is compulsory.');
	}else if(cityTextField.value==''){
		alert('City field is compulsory.');
	}else if(stateTextField.value==''){
		alert('State Field is compulsory.');
	}else{
	productLoader();
	}
});


function addUserToDB(userId){
	var db = Titanium.Database.install('UserDB.sqlite','UserTable');
	var rows = db.execute('INSERT INTO UserTable (UID, FirstName, LastName, Email, Phone, Street, PIN, City, State) VALUES("'+userId+'","'+firstNameTextField.value+'","'+lastNameTextField.value+'","'+emailTextField.value+'","'+phoneTextField.value+'","'+streetTextField.value+'","'+pinTextField.value+'","'+cityTextField.value+'","'+stateTextField.value+'")');
}






