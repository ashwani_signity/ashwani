var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var RegisterWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	navGroup: MainWin.navGroup,
	rootWindow : MainWin.rootWindow
});

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:436,
	width:'100%'
});
RegisterWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Alerts',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
MainWin.navGroup.close(MainWin);
});

headerView.add(backButton);

var infoLabel = Titanium.UI.createLabel({
	backgroundColor:'#e6e6e6',
	text:Titanium.App.Properties.getString('AlertName'),
	top:44,
	left:0,
	width:320,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'18'},
	
});
centerView.add(infoLabel);

var aboutUsImage = Titanium.UI.createImageView({
	url:'dummy.png',
	top:80,
	left:10,
	height:150,
	width:280
});
centerView.add(aboutUsImage);


var infoTextView = Titanium.UI.createTextArea({
	value:'Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
	top:250,
	left:10,
	height:170,
	width:300,
	textAlign:'left',
	editable:false
});
centerView.add(infoTextView);

MainWin.add(RegisterWin);

