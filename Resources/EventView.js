var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;
var EventWin = Titanium.UI.createWindow({
	
	backgroundColor:'#fff',
	navBarHidden:true
	
});

var eventGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:EventWin
});

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
EventWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Events',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

centerView.add(backButton);

var productItemSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
	backgroundColor:'#343434'
});
centerView.add(productItemSearchBar);

var eventData=[{title:'Jazz Sundays',desc:'Every Sunday',place:'Woombye Pub'},{title:'Summer Festival',desc:'2nd December 2012',place:'Palmwood Centre'},{title:'Book Workshop',desc:'From 26/01/2013',place:'Borders'}];
var rowData=[];
for(var i=0; i<eventData.length;i++){

	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
    // Now create each object to appear and add to row
    var productLbl = Ti.UI.createLabel({
        text: eventData[i].title,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10
    });
    productRow.add(productLbl);
    
    var productDes = Ti.UI.createLabel({
    	text: eventData[i].desc,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:12},
        left:60,
        top:40
    });
    productRow.add(productDes);
    
    var placelbl = Ti.UI.createLabel({
    	text:eventData[i].place,
    	textAlign:'left',
    	backgroundColor:'transparent',
    	font:{fontSize:12},
    	top:40,
    	left:180
    });
    productRow.add(placelbl);
    // Now we add the row to our data array
    rowData.push(productRow);
	
}

var eventTable = Titanium.UI.createTableView({
       data:rowData,
       top:94,
       height:336,
       rowHeight:70,
});
eventTable.addEventListener('click',function(e){
		
        Titanium.App.Properties.setString('EventName',eventData[e.index].title);
        var EventRegView = Titanium.UI.createWindow({
        	url:'EventRegister.js',
            navGroup : MainWin.navGroup,
            rootWindow : MainWin.currentWindow
        });
        MainWin.navGroup.open(EventRegView);
});
centerView.add(eventTable);
EventWin.add(centerView);
MainWin.add(eventGroup);

