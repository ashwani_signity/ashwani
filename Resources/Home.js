
var flag = false;

var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var centerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	heigth:'100%',
	width:'100%'
});



/****************************************************************************************************************
********************************************** Home View ********************************************************
****************************************************************************************************************/



var HomeWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	height:400,
	top:0,
	title:'Home'
});

HomeWin.add(centerView);

homeGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:HomeWin
});

var LoginButton = Titanium.UI.createView({
     backgroundColor:'#ff6000',
	height:'8%',
	width:'18.81%',
	borderRadius:'5'
});
LoginButton.addEventListener('click',function(){
	
	var loginPage=Titanium.UI.createWindow({
		title:'Register/Login',
		url:'LogInRegisterFormPage.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : homeGroup,
        rootWindow : HomeWin
	});
	//MainWin.close();
	homeGroup.open(loginPage);
	
});
centerView.add(LoginButton);
var LoginLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Login',
	textAlign:'center',
	color:'#FFFFFF'
});
LoginButton.add(LoginLabel);


if(Titanium.App.Properties.getString('LoginUser')==null){
var loginAlert = Titanium.UI.createAlertDialog({
	title:'Create Account or Log in',
	message: 'Create a free account or log in to activate enhanced features and faster use.', 
	buttonNames: ['Not now','Ok, continue']
});
loginAlert.addEventListener('click',function(e){
	
	if(e.index==1){
		
		var RegWin=Titanium.UI.createWindow({	
		url:'LogInRegisterFormPage.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : homeGroup,
        rootWindow : HomeWin
	});
	homeGroup.open(RegWin);
	}
	
});
loginAlert.show();
}

/****************************************************************************************************************
********************************************** Product View *****************************************************
****************************************************************************************************************/


/************************* API Connection   ************************/

function productLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

ProductWin.add(transview);
//MainWin.remove(transview);
var xhr = Titanium.Network.createHTTPClient();
//var url = Titanium.App.Properties.getString("baseurl")+'?operation=categories&webSiteId=7';
var url = Titanium.App.Properties.getString("baseurl");
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateProductTable(str_res);
			  ProductWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      ProductWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
ProductWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'categories',webSiteId:'7'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}


var ProductWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	height:430,
	top:0,
	title:'Products',
	navBarHidden:true,
    
});
// ProductWin.addEventListener('focus',function(e){
	// productLoader();
// });
productLoader();

var productHeaderLabel = Titanium.UI.createLabel({
           backgroundColor : '#1bc6ff',
           color:'#fff',
           font:{fontWeight:'bold',fontSize:'20'},
           top:0,
           width:320,
           height:44,
           textAlign:'center',
           text:'Products'	
});
ProductWin.add(productHeaderLabel);

var cartButton = Titanium.UI.createLabel({
	height:30,
	width:80,
	top:7,
	right:10,
	text:'Cart',
	color:'#fff',
	backgroundColor:'green',
	textAlign:'center',
	borderRadius:'7',
	font:{fontWeight:'bold',fontSize:16}
});
cartButton.addEventListener('click',function(){
     
     var CartView = Ti.UI.createWindow({
				     url:'ShoppingCartView.js',
				 _parent: Titanium.UI.currentWindow,
		        navGroup: productGroup,
		     rootWindow : ProductWin,
		     
			});
			productGroup.open(CartView);
     
});
ProductWin.add(cartButton);

var productSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
});
ProductWin.add(productSearchBar);

function populateProductTable(str_res){
	
var productData=[];

for(var i=0; i<str_res.data.length;i++){
//alert(i);
  //alert(str_res.data[i].category_name);
	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
    // Now create each object to appear and add to row
    var catImage = Ti.UI.createImageView({
    	url:str_res.data[i].category_image,
    	top:5,
    	left:5,
    	height:60,
    	width:50
    });
    productRow.add(catImage);
    
    var productLbl = Ti.UI.createLabel({
        text: str_res.data[i].category_name,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10
    });
    productRow.add(productLbl);
    
    
    var productDes = Ti.UI.createLabel({
    	text: str_res.data[i].category_detail,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:60,
        top:40,
        height:30,
        width:200
    });
    productRow.add(productDes);
    // Now we add the row to our data array
    productData.push(productRow);
	

}

var productTable = Titanium.UI.createTableView({
       data:productData,
       my_id:'100',
       top:94,
       height:336,
       rowHeight:70,
});

productTable.addEventListener('click',function(e){
		Titanium.App.Properties.setString('Category',str_res.data[e.index].category_name);
		Titanium.App.Properties.setString('CategoryId',str_res.data[e.index].category_id);
		var ProductItemView=Titanium.UI.createWindow({	
		url:'ProductItem.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : productGroup,
        rootWindow : ProductWin
	});
	productGroup.open(ProductItemView);
        
});
ProductWin.add(productTable);
}


productGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:ProductWin
});



/****************************************************************************************************************
********************************************** Services View ****************************************************
****************************************************************************************************************/

/************************* API Connection   ************************/

function serviceProductLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

ServicesWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/services.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateServiceTable(str_res);
			  ServicesWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      ServicesWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
ServicesWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'services',webSiteId:'7'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}


function serviceProLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

ServicesWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/services.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateServiceTable(str_res);
			  ServicesWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      ServicesWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
ServicesWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'service_provider',webSiteId:'7'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}




var ServicesWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	height:430,
	top:0,
	title:'Services',
	navBarHidden:true
});
// ServicesWin.addEventListener('focus',function(){
	// serProButton.url='serviceProvider-inactive.png';
	// serButton.url='service-active.png';
	// serviceProductLoader();
// });

serviceProductLoader();

var servicesHeaderLabel = Titanium.UI.createLabel({
           backgroundColor : '#1bc6ff',
           color:'#fff',
           font:{fontWeight:'bold',fontSize:'20'},
           top:0,
           width:320,
           height:44,
           textAlign:'center',
           text:'Services'	
});
ServicesWin.add(servicesHeaderLabel);

var serButton = Titanium.UI.createImageView({
	url:'service-active.png',
	backgroundColor:'transparent',
	top:52,
	left:10,
	height:35,
	width:150
});
//ServicesWin.add(serButton);
serButton.addEventListener('click',function(e){
	serProButton.url='serviceProvider-inactive.png';
	serButton.url='service-active.png';
	serviceProductLoader();
});


var serProButton = Titanium.UI.createImageView({
	url:'serviceProvider-inactive.png',
	backgroundColor:'transparent',
	top:52,
	left:160,
	height:35,
	width:150
});

//ServicesWin.add(serProButton);
serProButton.addEventListener('click',function(e){
	serButton.url='service-inactive.png';
	serProButton.url='serviceProvider-active.png';
	serviceProLoader();
});


var servicesSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
});
ServicesWin.add(servicesSearchBar);

function populateServiceTable(str_res){
var serviceData=[];



for(var i=0; i<str_res.data.length;i++){

	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
 if(str_res.data[i].type=='service'){
 
   var serviceName = Ti.UI.createLabel({
   	   left:10,
   	   height:30,
   	   width:200,
   	   text:str_res.data[i].service_name,
   	   color:'#000',
   	   font:{fontWeight:'bold',fontSize:16}
   });
    productRow.add(serviceName);
    
    var serviceCost = Ti.UI.createLabel({
       right:10,
   	   height:30,
   	   text:'$'+str_res.data[i].price,
   	   color:'#ff6000',
   	   font:{fontWeight:'bold',fontSize:14}
    });
    productRow.add(serviceCost);
    
   }else{
   	var proImageView = Ti.UI.createImageView({
   		url:str_res.data[i].provider_image,
    	top:5,
    	left:5,
    	height:60,
    	width:50
   	});
   	productRow.add(proImageView);
   	
   	var proName = Ti.UI.createLabel({
   	   left:70,
   	   height:30,
   	   width:200,
   	   top:5,
   	   text:str_res.data[i].provider_name,
   	   color:'#000',
   	   font:{fontWeight:'bold',fontSize:16}
   	});
   	productRow.add(proName);
   	
   	var proService = Ti.UI.createLabel({
   	   left:70,
   	   height:30,
   	   width:200,
   	   top:35,
   	   text:str_res.data[i].services,
   	   color:'#ff6000',
   	   font:{fontWeight:'bold',fontSize:14}
   	});
   	productRow.add(proService);
   }
    serviceData.push(productRow);

   }


var ServiceTable = Titanium.UI.createTableView({
       data:serviceData,
       top:94,
       height:280,
       rowHeight:70,
});
ServiceTable.addEventListener('click',function(e){
	
	 if(str_res.data[e.index].type == 'service'){
	 	
	 	
	 Ti.App.Properties.setString('SerId',str_res.data[e.index].service_id);
	 Ti.App.Properties.setString('SerName',str_res.data[e.index].service_name)
	 	var ServiceProView=Titanium.UI.createWindow({	
		url:'ProviderView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : servicesGroup,
        rootWindow : ServicesWin
	});
	//bookingWinArray.push(ServiceProView);
	servicesGroup.open(ServiceProView);
	 }else{
	 	
	Titanium.App.Properties.setString('technician',str_res.data[e.index].provider_name);
	Titanium.App.Properties.setString('techImage',str_res.data[e.index].provider_image);
	Titanium.App.Properties.setString('techSer',str_res.data[e.index].services);
	
	var ProInfoView=Titanium.UI.createWindow({	
		url:'ProviderInfo.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : servicesGroup,
        rootWindow : ServicesWin
	});
	servicesGroup.open(ProInfoView);
	 	
	 }
	
});
ServicesWin.add(ServiceTable);

}



/*
var tableServiceData=[{title:'Hair Salon',desc:'Hair style for men and women.'}];

var serviceData=[];

for(var i=0; i<tableServiceData.length;i++){

	var serviceRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
    // Now create each object to appear and add to row
    var serviceLbl = Ti.UI.createLabel({
        text: tableServiceData[i].title,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10
    });
    serviceRow.add(serviceLbl);
    
    var serviceDes = Ti.UI.createLabel({
    	text: tableServiceData[i].desc,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:60,
        top:40
    });
    serviceRow.add(serviceDes);
    // Now we add the row to our data array
    serviceData.push(serviceRow);
	
}

var servicesTable = Titanium.UI.createTableView({
       data:serviceData,
       top:94,
       height:336,
       rowHeight:70,
});

servicesTable.addEventListener('click',function(e){
	
	Titanium.App.Properties.setString('serviceItem',tableServiceData[e.index].title);
	var ServiceItemView=Titanium.UI.createWindow({	
		url:'ServicesItem.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : servicesGroup,
        rootWindow : ServicesWin
	});
	servicesGroup.open(ServiceItemView);
	
});

ServicesWin.add(servicesTable);

*/
servicesGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:ServicesWin
});




/****************************************************************************************************************
********************************************** Bookings View ****************************************************
****************************************************************************************************************/



var BookingsWin = Titanium.UI.createWindow({
	backgroundColor:'#fff',
	height:430,
	top:0,
	navBarHidden:true
});
// BookingsWin.addEventListener('focus',function(){
	// eventLoader();
// });

eventLoader();

var bookingHeaderLabel = Titanium.UI.createLabel({
           backgroundColor : '#1bc6ff',
           color:'#fff',
           font:{fontWeight:'bold',fontSize:'20'},
           top:0,
           width:320,
           height:44,
           textAlign:'center',
           text:'Events'	
});
BookingsWin.add(bookingHeaderLabel);
var eventSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
	backgroundColor:'#343434'
});
BookingsWin.add(eventSearchBar);


function eventLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

BookingsWin.add(transview);
//MainWin.remove(transview);
var xhr = Titanium.Network.createHTTPClient();
//var url = Titanium.App.Properties.getString("baseurl")+'?operation=categories&webSiteId=7';
var url = 'http://lovemyshopping.com.au/webservices/events.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateEventTable(str_res);
              
			  BookingsWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      BookingsWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
BookingsWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'event_list',webSiteId:'7'});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}


function populateEventTable(str_res){
var eventData=[];

//Ti.API.info('value------>'+'  '+str_res);

for(var i=0; i<str_res.data.length;i++){
	
	
	var eventRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
    // Now create each object to appear and add to row
    var productLbl = Ti.UI.createLabel({
        text: str_res.data[i].event_title,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10
    });
    eventRow.add(productLbl);
    
    var productDes = Ti.UI.createLabel({
    	text: str_res.data[i].start_time,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:12},
        left:60,
        top:40
    });
    eventRow.add(productDes);
    
    var placelbl = Ti.UI.createLabel({
    	text:str_res.data[i].venue,
    	textAlign:'left',
    	backgroundColor:'transparent',
    	font:{fontSize:12},
    	top:40,
    	left:180
    });
    eventRow.add(placelbl);
  
    // Now we add the row to our data array
    eventData.push(eventRow);
	
   }

var eventTable = Titanium.UI.createTableView({
       data:eventData,
       top:94,
       height:336,
       rowHeight:70,
});
eventTable.addEventListener('click',function(e){
		
        Titanium.App.Properties.setString('EventName',str_res.data[e.index].event_title);
        Titanium.App.Properties.setString('EventDec',str_res.data[e.index].event_detail);
        var EventRegView = Titanium.UI.createWindow({
        	url:'EventRegister.js',
            _parent: Titanium.UI.currentWindow,
            navGroup : bookingGroup,
            rootWindow : BookingsWin
        });
        bookingGroup.open(EventRegView);
});
BookingsWin.add(eventTable);
	
}



bookingGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:BookingsWin
});

/****************************************************************************************************************
********************************************** More View ****************************************************
****************************************************************************************************************/

var MoreWin = Titanium.UI.createWindow({
	backgroundColor:'#f1f1f1',
	height:430,
	top:0,
	title:'More',
	navBarHidden:true
});


var moreHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	textAlign:'center',
	text:'More',
	font:{fontWeight:'bold',fontSize:20},
	top:0,
	left:0,
	height:44,
	width:320
});
MoreWin.add(moreHeader);


var aboutusButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'About-us.png',
	height:60,
	width:80,
	top:65,
	left:29
});
aboutusButton.addEventListener('click',function(e){
	var AboutView=Titanium.UI.createWindow({	
		url:'AboutUs.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(AboutView);
});

MoreWin.add(aboutusButton);


var contactButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Contact.png',
	height:60,
	width:80,
	top:65,
	left:126
});
contactButton.addEventListener('click',function(e){
	var ContactView=Titanium.UI.createWindow({	
		url:'ContactUs.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(ContactView);
});
MoreWin.add(contactButton);


var directButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Directions.png',
	height:60,
	width:80,
	top:65,
	left:223
});
directButton.addEventListener('click',function(){
	var MapView=Titanium.UI.createWindow({	
		url:'Directions.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(MapView);
});
MoreWin.add(directButton);


var locationButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'locations.png',
	height:60,
	width:80,
	top:175,
	left:29
});

locationButton.addEventListener('click',function(e){
	
	var LocationView=Titanium.UI.createWindow({	
		url:'Location.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(LocationView);

});

MoreWin.add(locationButton);

var videoButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'videos.png',
	height:60,
	width:80,
	top:175,
	left:126
});

videoButton.addEventListener('click',function(e){
	
	var VideoView=Titanium.UI.createWindow({	
		url:'VideoView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(VideoView);

});


MoreWin.add(videoButton);


var rewardButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Rewards.png',
	height:60,
	width:80,
	top:175,
	left:223
});

rewardButton.addEventListener('click',function(e){
	
	var RewardView=Titanium.UI.createWindow({	
		url:'Rewards.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(RewardView);

});

MoreWin.add(rewardButton);


/*
var eventButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Events.png',
	height:53,
	width:68,
	top:65,
	left:126
});
eventButton.addEventListener('click',function(e){
	var EventView=Titanium.UI.createWindow({	
		url:'EventView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(EventView);
});

MoreWin.add(eventButton);

*/

var alertButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Alerts.png',
	height:60,
	width:80,
	top:285,
	left:29
});

alertButton.addEventListener('click',function(e){
	var AlertView=Titanium.UI.createWindow({	
		url:'AlertsView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(AlertView);
});

MoreWin.add(alertButton);








var shareButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'share.png',
	height:60,
	width:80,
	top:285,
	left:126
});
shareButton.addEventListener('click',function(e){
	var ShareView=Titanium.UI.createWindow({	
		url:'ShareView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(ShareView);
});


MoreWin.add(shareButton);


var settingsButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Settings.png',
	height:60,
	width:80,
	top:285,
	left:223
});

settingsButton.addEventListener('click',function(e){
	var SettingsView=Titanium.UI.createWindow({	
		url:'SettingView.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : moreGroup,
        rootWindow : MoreWin
	});
	moreGroup.open(SettingsView);
});

MoreWin.add(settingsButton);


moreGroup = Titanium.UI.iPhone.createNavigationGroup({
	window:MoreWin
});

//############################################ Creating TabBarView ###########################################//
var tabButtonView = Titanium.UI.createView({
	backgroundColor:'#101987',
	bottom:'0%',
	height:'10.41%',
	width:'100%'
});

//#############################################Adding Home Button#############################################//
var homeButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Home_active.png',
	left:'0%',
	heigth:'100%',
	width:'20%'
	
});
homeButton.addEventListener('click',function(e){
	homeGroup.show();
	productGroup.hide();
	servicesGroup.hide();
	bookingGroup.hide();
	moreGroup.hide();
	homeButton.backgroundImage='Home_active.png';
	productButton.backgroundImage='Products.png';
	servicesButton.backgroundImage='Services.png';
	bookingsButton.backgroundImage='events_inactive.png';
	moreButton.backgroundImage='More.png';
});

tabButtonView.add(homeButton);

//############################################Adding Products Button#########################################//
var productButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Products.png',
	left:'20%',
	heigth:'100%',
	width:'20%'
	
});
productButton.addEventListener('click',function(e){
	Ti.API.info('Product Clicked!!');
		
		homeGroup.hide();
		productGroup.show();
		servicesGroup.hide();
		bookingGroup.hide();
		moreGroup.hide();
        homeButton.backgroundImage='Home.png';
	    productButton.backgroundImage='Products_active.png';   
	    servicesButton.backgroundImage='Services.png';
	    bookingsButton.backgroundImage='events_inactive.png';  
	    moreButton.backgroundImage='More.png';
	    
	    if(Titanium.App.Properties.getString('productFlag')=='yes'){
	    Titanium.App.Properties.setString('productFlag','no');
	   // productLoader();
	    }
	    
	    
});
tabButtonView.add(productButton);

//##############################################Adding Services Button#######################################//

var servicesButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'Services.png',
	left:'40%',
	heigth:'100%',
	width:'20%'
});

servicesButton.addEventListener('click',function(e){
	    homeGroup.hide();
		productGroup.hide();
		servicesGroup.show();
		bookingGroup.hide();
		moreGroup.hide();
        homeButton.backgroundImage='Home.png';
	    productButton.backgroundImage='Products.png';   
	    servicesButton.backgroundImage='Services_active.png';
	    bookingsButton.backgroundImage='events_inactive.png';
	    moreButton.backgroundImage='More.png';
});

tabButtonView.add(servicesButton);

//###############################################Adding Booking Button######################################//
var bookingsButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'events_inactive.png',
	left:'60%',
	heigth:'100%',
	width:'20%'
	
});
bookingsButton.addEventListener('click',function(e){
       
        homeGroup.hide();
		productGroup.hide();
		servicesGroup.hide();
		bookingGroup.show();
		moreGroup.hide();
        homeButton.backgroundImage='Home.png';
	    productButton.backgroundImage='Products.png';   
	    servicesButton.backgroundImage='Services.png';
	    bookingsButton.backgroundImage='events_active.png';
	    moreButton.backgroundImage='More.png';
});

tabButtonView.add(bookingsButton);

//####################################################Adding More Button#######################################//
var moreButton = Titanium.UI.createView({
	backgroundColor:'transparent',
	backgroundImage:'More.png',
	left:'80%',
	heigth:'100%',
	width:'20%'
	
});
moreButton.addEventListener('click',function(e){
	
	homeGroup.hide();
	productGroup.hide();
	servicesGroup.hide();
	bookingGroup.hide();
	moreGroup.show();
	homeButton.backgroundImage='Home.png';
	productButton.backgroundImage='Products.png';
	servicesButton.backgroundImage='Services.png';
	bookingsButton.backgroundImage='events_inactive.png';
	moreButton.backgroundImage='More_active.png';
	
});
tabButtonView.add(moreButton);

moreGroup.hide();
bookingGroup.hide();
servicesGroup.hide();
productGroup.hide();
MainWin.add(moreGroup);
MainWin.add(bookingGroup);
MainWin.add(servicesGroup);
MainWin.add(productGroup);
MainWin.add(homeGroup);

MainWin.add(tabButtonView);


