var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);

var productItemHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center',
	text:'Choose technician'
});
centerView.add(productItemHeader);

var backButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
centerView.add(backButton);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

var productItemSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
	backgroundColor:'#343434'
});
centerView.add(productItemSearchBar);


//######################################### API Connection #################################//


function serviceProLoader() {
//loader = function(){
	
var transview=Titanium.UI.createView({
top:'0%',
height:'100%',
width:'100%',
zIndex:1000,
opacity:0.7,
backgroundColor:'#000'
});


bfforloader_image = Titanium.UI.createImageView({
backgroundColor:'#000',
top:'46.5%',
height:'12.5%',
opacity:1.0,
borderRadius:10,
borderColor:'white',
width:'40%'
});
transview.add(bfforloader_image);


var actInd = Titanium.UI.createActivityIndicator({
top:'51%',
font: {fontFamily:'Helvetica Neue', fontSize:15,fontWeight:'bold'},
color: 'white',
message: 'Loading...'
});
transview.add(actInd);

actInd.show();

MainWin.add(transview);
var xhr = Titanium.Network.createHTTPClient();
var url = 'http://lovemyshopping.com.au/webservices/services.php';
Ti.API.info(url);
var method = 'POST'; 

xhr.onload = function() {
   if(xhr.readyState ==4) {
  try {
      if(this.responseText != '{}') {
              var str_res = JSON.parse(this.responseText);
              Ti.API.info(str_res);
              populateServiceTable(str_res);
			  MainWin.remove(transview);
   	  
      }
       } catch(E) {
      Titanium.App.Properties.setString('surprise_me', 'false');
      var a=Titanium.UI.createAlertDialog();
      a.message ='No Record Found.';
      a.buttonNames = ['ok'];
      a.show();
      MainWin.remove(transview);
       }
   }
};

xhr.onerror = function() {
alert('Error in network connection.');
MainWin.remove(transview);
};
xhr.open(method, url);
//xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
xhr.send({operation:'services_detail',webSiteId:'7',service_id:Ti.App.Properties.getString('SerId')});
/*setTimeout(function(){
	apiData();
},1000);*/
	
}
serviceProLoader();

function populateServiceTable(str_res){
var serviceData=[];

for(var i=0; i<str_res.data.length;i++){

	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
 
   	var proImageView = Ti.UI.createImageView({
   		url:str_res.data[i].provider_image,
    	top:5,
    	left:5,
    	height:60,
    	width:50
   	});
   	productRow.add(proImageView);
   	
   	var proName = Ti.UI.createLabel({
   	   left:70,
   	   height:30,
   	   width:200,
   	   top:5,
   	   text:str_res.data[i].provider_name,
   	   color:'#000',
   	   font:{fontWeight:'bold',fontSize:16}
   	});
   	productRow.add(proName);
   	
   	var proService = Ti.UI.createLabel({
   	   left:70,
   	   height:30,
   	   width:200,
   	   top:35,
   	   text:str_res.data[i].services,
   	   color:'#ff6000',
   	   font:{fontWeight:'bold',fontSize:14}
   	});
   	productRow.add(proService);
   
    serviceData.push(productRow);

    }


var ServiceTable = Titanium.UI.createTableView({
       data:serviceData,
       top:94,
       height:280,
       rowHeight:70,
});
ServiceTable.addEventListener('click',function(e){
	
	Titanium.App.Properties.setString('technician',str_res.data[e.index].provider_name);
	Titanium.App.Properties.setString('techImage',str_res.data[e.index].provider_image);
	Titanium.App.Properties.setString('techSer',str_res.data[e.index].services);
	Titanium.App.Properties.setString('techId',str_res.data[e.index].provider_Id);
	
	var ProInfoView=Titanium.UI.createWindow({	
		url:'ProviderInfo.js',
		_parent: Titanium.UI.currentWindow,
        navGroup : MainWin.navGroup,
        rootWindow : MainWin.currentWindow
	});
	MainWin.navGroup.open(ProInfoView);
	
});
centerView.add(ServiceTable);

}










