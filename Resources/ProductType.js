

var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);

var productItemHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center'
});
centerView.add(productItemHeader);

var scartButton = Titanium.UI.createLabel({
	height:30,
	width:80,
	top:7,
	right:10,
	text:'Cart',
	color:'#fff',
	backgroundColor:'green',
	textAlign:'center',
	borderRadius:'7',
	font:{fontWeight:'bold',fontSize:16}
});
scartButton.addEventListener('click',function(e){
	var CartView = Ti.UI.createWindow({
				     url:'ShoppingCartView.js',
		        navGroup: MainWin.navGroup,
		     rootWindow : MainWin.rootWindow
			});
			MainWin.navGroup.open(CartView);
});

centerView.add(scartButton);


var backButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
centerView.add(backButton);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});


var productInfoView = Titanium.UI.createView({
	backgroundColor:'gray',
	width:'100%',
	height:44,
	top:44,
});
centerView.add(productInfoView);

var productName = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:Titanium.App.Properties.getString('ProductName'),
	textAlign:'left',
	color:'#000',
	font:{fontWeight:'bold',fontSize:16},
	height:30,
	left:5,
	top:7
});
productInfoView.add(productName);

var productPrice = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'$'+Titanium.App.Properties.getString('ProductPrice'),
	textAlign:'right',
	color:'#ff6000',
	font:{fontWeight:'bold',fontSize:12},
	height:30,
	right:5,
	top:7
});
productInfoView.add(productPrice);

var productBgScroll = Ti.UI.createScrollView({
	backgroundColor : 'transparent',
	top:88,
	height:342,
	width:320,
	
});
centerView.add(productBgScroll);

var imgArray = Ti.App.Properties.getList('ProductImages');

var productImage1 = Ti.UI.createImageView({
	url:imgArray[0].image1,
	top:0,
	// left:85,
	width:150,
	height:150
});


var productImage2 = Ti.UI.createImageView({
	url:imgArray[0].image2,
	top:0,
	// left:85,
	width:150,
	height:150
});

var productImage3 = Ti.UI.createImageView({
	url:imgArray[0].image3,
	top:0,
	// left:85,
	width:150,
	height:150
});

var imgScrollView = Ti.UI.createScrollableView({
	views:[productImage1,productImage2,productImage3],
	top:5,
	left:0,
	width:320,
	height:150,
	scrollingEnabled:true
});
productBgScroll.add(imgScrollView);

var brandValue = Ti.UI.createLabel({
	backgroundColor:'transparent',
	top:150,
	left:10,
	height:40,
	text:Titanium.App.Properties.getString('ProductBrand'),
	color:'#000'
});
productBgScroll.add(brandValue);

var QuntityLabel = Ti.UI.createLabel({
	backgroundColor:'transparent',
	top:150,
	right:10,
	height:40,
	text:Titanium.App.Properties.getString('ProductQuantity'),
	color:'#000'
});
productBgScroll.add(QuntityLabel);

var cartButton = Ti.UI.createLabel({
	backgroundColor:'green',
	top:185,
	left:50,
	height:40,
	width:220,
	text:'Add to Cart',
	color:'#fff',
	textAlign:'center',
	font:{fontWeight:'bold',fontSize:18},
	borderRadius:'7'
});
productBgScroll.add(cartButton);



cartButton.addEventListener('click',function(e){
	
	
	var productArray =({ProductId:Titanium.App.Properties.getString('ProductId'), ProductName:Titanium.App.Properties.getString('ProductName'), ProductPrice:Titanium.App.Properties.getString('ProductPrice'), ProductQuan:Titanium.App.Properties.getString('ProductQuantity'),ProductImage:Titanium.App.Properties.getString('ProductImage'),ProductNumber:'1'});	
	var shoppingCart2 = [];
	if(Titanium.App.Properties.getList('ShoppingCart')==null){
		
	}else{
		shoppingCart2 = Titanium.App.Properties.getList('ShoppingCart');
	}

    var cartFlag = false;
    for (var i=0; i<shoppingCart2.length; i++){
    	if(Titanium.App.Properties.getString('ProductId')==shoppingCart2[i].ProductId){
    		cartFlag=true;
    		break;
    	}
    }

	if(cartFlag==true){
		
		var alertDialog = Ti.UI.createAlertDialog({
			message:'Item already exists.',
			buttonNames:['Ok']
		});
		alertDialog.show();
		
	}else{
	
	shoppingCart2.push(productArray);
	Titanium.App.Properties.setList('ShoppingCart',shoppingCart2);
	
	var cartDialog = Ti.UI.createAlertDialog({
		title:'Add to cart',
		message:'One of '+Titanium.App.Properties.getString('ProductName')+' has been added to your cart!',
		buttonNames:['Keep shopping','Checkout']
	});
	
	cartDialog.addEventListener('click',function(e){
		if(e.index==1){
			Ti.API.info(Titanium.App.Properties.getList('ShoppingCart'));
			var CartView = Ti.UI.createWindow({
				     url:'ShoppingCartView.js',
		        navGroup: MainWin.navGroup,
		     rootWindow : MainWin.rootWindow
			});
			MainWin.navGroup.open(CartView);
		}
	});
	
	cartDialog.show();
	}
});


// open window
