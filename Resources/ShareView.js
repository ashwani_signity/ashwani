var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;
MainWin.navBarHidden=true;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);
//############################################Header View##################################################//

var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Share',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

centerView.add(backButton);

var infoLabel = Titanium.UI.createLabel({
	backgroundColor:'#e6e6e6',
	text:'<Business name>',
	top:44,
	left:0,
	width:320,
	textAlign:'left',
	font:{fontWeight:'bold',fontSize:'18'},
	
});
centerView.add(infoLabel);

var emailButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:200,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(emailButton);

var emailText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Share by Email',
    color:'#fff'
});
emailButton.add(emailText);

var smsButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:260,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(smsButton);

var smsText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Share by SMS',
    color:'#fff'
});
smsButton.add(smsText);

var facebookButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:320,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(facebookButton);

var facebookText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Share by Facebook',
    color:'#fff'
});
facebookButton.add(facebookText);

var twitterButton = Titanium.UI.createView({
	backgroundColor:'#758aa7',
	top:380,
	width:190,
	height:40,
	borderRadius:'7'
});
centerView.add(twitterButton);

var twitterText = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Share by Twitter',
    color:'#fff'
});
twitterButton.add(twitterText);




