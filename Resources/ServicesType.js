
var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);

var productItemHeader = Titanium.UI.createLabel({
	backgroundColor:'#1bc6ff',
	color:'#fff',
	font:{fontWeight:'bold',fontSize:'20'},
	top:0,
	width:320,
	height:44,
	textAlign:'center',
	text:Titanium.App.Properties.getString('TypeService')
});
centerView.add(productItemHeader);

var backButton = Titanium.UI.createLabel({
	backgroundColor:'#ff6000',
	color:'#fff',
	top:7,
	left:5,
	height:30,
	width:45,
	text:'Back',
	textAlign:'center',
	borderRadius:'7'
});
centerView.add(backButton);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

var productItemSearchBar = Titanium.UI.createSearchBar({
	top:44,
	width:320,
	height:50,
	backgroundColor:'#343434'
});
centerView.add(productItemSearchBar);
//#################
//##################################################  Table View  #############################################//
//#################
var tableData=[{title:'Long Style',desc:'Lorem lipsum Lorem lipsum',price:'$99.00'},{title:'Short Style',desc:'Lorem lipsum Lorem lipsum',price:'$49.00'},{title:'Touch Up',desc:'Lorem lipsum Lorem lipsum',price:'$39.00'}];

var productData=[];

for(var i=0; i<tableData.length;i++){

	var productRow = Ti.UI.createTableViewRow({    
        hasChild:true,     
        backgroundColor : '#fff'       
    });
 
    // Now create each object to appear and add to row
    var productLbl = Ti.UI.createLabel({
        text: tableData[i].title,
        textAlign: 'left',
        font:{fontWeight:'bold',fontSize:18},
        backgroundColor:'transparent',
        left:60,
        top:10,
        width:180,
        height:25
    });
    productRow.add(productLbl);
    
    var productDes = Ti.UI.createLabel({
    	text: tableData[i].desc,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:60,
        top:30,
        height:25,
        width:200
    });
    productRow.add(productDes);
    
    var productQuan = Ti.UI.createLabel({
    	text: tableData[i].quan,
        textAlign: 'left',
        backgroundColor:'transparent',
        font:{fontSize:14},
        left:250,
        top:10
    });
    productRow.add(productQuan);
    
    var productPrice = Ti.UI.createLabel({
    	text: tableData[i].price,
        textAlign: 'left',
        backgroundColor:'transparent',
        color:'#ff6000',
        font:{fontSize:14},
        left:60,
        top:50
    });
    productRow.add(productPrice);
    // Now we add the row to our data array
    productData.push(productRow);
	
}

var productTable = Titanium.UI.createTableView({
       data:productData,
       my_id:'100',
       top:94,
       height:336,
       rowHeight:70,
});
productTable.addEventListener('click',function(e){
		// Titanium.App.Properties.setString('Item','Hair');
		// var preferwin=Titanium.UI.createWindow({	
		// url:'Servies.js',
	// });
	// preferwin.open({fullscreen:false});
//         
});
centerView.add(productTable);


// open window