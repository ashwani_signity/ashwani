var MainWin=Titanium.UI.currentWindow;
Titanium.UI.setBackgroundColor('#000');
MainWin.fullscreen = false;
MainWin.navBarHidden = true;

var centerView = Titanium.UI.createView({
	backgroundColor:'#FFFFFF',
	heigth:'100%',
	width:'100%'
});
MainWin.add(centerView);
var headerView = Titanium.UI.createView({
	backgroundColor:'#1bc6ff',
	top:'0%',
	height:'9.16%',
	width:'100%'
});
centerView.add(headerView);

//##HeaderText##//

var headerLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Settings',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
headerView.add(headerLabel);

var backButton = Titanium.UI.createView({
	backgroundColor:'#ff6000',
	height:31,
	width:45,
	top:7,
	left:5,
	borderRadius:'7'
});

var backLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	text:'Back',
	height:'100%',
	width:'100%',
	textAlign:'center',
	color:'#fff'
});
backButton.add(backLabel);
backButton.addEventListener('click',function(e){
	MainWin.navGroup.close(MainWin);
});

centerView.add(backButton);


//############################################Register Text Field##########################################//
//##Register Label##//
var registerLabel = Titanium.UI.createLabel({
	backgroundColor:'#FFFFFF',
	height:'5%',
	width:'50%',
	left:'4%',
	top:60,
	text:'Your Details',
	textAlign:'left',
	color:'#ff6000',	
	font:{fontWeight:'bold',fontSize:'20'}
});
centerView.add(registerLabel);
//##First Name Field##//
var firstNameTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:90,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'First Name'
});
centerView.add(firstNameTextField);
//##Last Name Field##//
var lastNameTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:90,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Last Name'
});
centerView.add(lastNameTextField);
//##EMail Field##//
var emailTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:125,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'E-mail'
});
centerView.add(emailTextField);
//##Phone Number Field##//
var phoneTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:125,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Phone Number'
});
centerView.add(phoneTextField);
//##Street Addresss Field##//
var streetTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'60%',
	top:160,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Street Address'
});
centerView.add(streetTextField);
//##Pin Field##//
var pinTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'20%',
	top:160,
	left:'67%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'4554'
});
centerView.add(pinTextField);

//##City Field##//
var cityTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:195,
	left:'4%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'City/Suburb'
});
centerView.add(cityTextField);










//##State Field##//
var stateTextField = Titanium.UI.createTextField({
	height:'6%',
	width:'40%',
	top:195,
	left:'47%',
	borderColor:'#000000',
	borderRadius:'5',
	hintText:'Select State'
});
centerView.add(stateTextField);

//## App Settings ##//
var settingLabel = Titanium.UI.createLabel({
	backgroundColor:'#FFFFFF',
	height:'5%',
	width:'50%',
	left:'4%',
	top:230,
	text:'App Settings',
	textAlign:'left',
	color:'#ff6000',	
	font:{fontWeight:'bold',fontSize:'20'}
});
centerView.add(settingLabel);



//##Save Button##//
var saveButton = Titanium.UI.createView({
     backgroundColor:'#758aa7',
	// left:'1.56%',
	top:380,
	height:'8%',
	width:'55%',
	borderRadius:'5'

});
centerView.add(saveButton);

var savebuttonLabel = Titanium.UI.createLabel({
	backgroundColor:'transparent',
	height:'100%',
	width:'100%',
	text:'Save',
	textAlign:'center',
	color:'#FFFFFF',
	font:{fontWeight:'bold',fontSize:'20'}
});
saveButton.add(savebuttonLabel);

